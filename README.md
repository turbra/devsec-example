# DevSecOps CI/CD Pipeline

This repository contains a modular CI/CD pipeline designed with DevSecOps principles in mind. Each stage of the pipeline emphasizes security checks and practices to ensure that software is not only developed and deployed rapidly but also securely.

## Project Structure

```plaintext
.
├── .gitlab-ci.yml       # Main CI/CD configuration
├── README.md            # This documentation
└── .templates
    ├── binary-sca.yml   # Post-build Binary SCA checks
    ├── dast.yml         # Dynamic Application Security Testing
    ├── iast.yml         # Interactive Application Security Testing
    ├── oast.yml         # Out-of-band Application Security Testing
    ├── rasp.yml         # Post-deploy Runtime Application Self-Protection
    ├── sast.yml         # Pre-build Static Application Security Testing
    └── sca.yml          # Pre-build Software Composition Analysis
```

## Using the Pipeline Templates

Developers can easily customize their pipeline by including specific stages or jobs from the `.templates` directory in their `.gitlab-ci.yml` file.

### How to Include a Template

To include a template, add the following to your `.gitlab-ci.yml`:

```yaml
include:
  - '.templates/[template-name].yml'
```

Replace `[template-name]` with the name of the desired template (e.g., `sast`, `dast`).

### Available Templates

1. **SAST (Static Application Security Testing)**: Checks the source code for vulnerabilities before it enters the version control system. Refer to `sast.yml`.
  
2. **SCA (Software Composition Analysis)**: Checks for vulnerabilities in open-source components. Refer to `sca.yml`.
  
3. **Binary SCA**: Post-build checks on the software binaries. Refer to `binary-sca.yml`.
  
4. **DAST (Dynamic Application Security Testing)**: Tests running applications for vulnerabilities. Refer to `dast.yml`.
  
5. **IAST (Interactive Application Security Testing)**: Combines aspects of both SAST and DAST. Refer to `iast.yml`.
  
6. **OAST (Out-of-band Application Security Testing)**: Detects vulnerabilities that traditional testing might miss. Refer to `oast.yml`.
  
7. **RASP (Runtime Application Self-Protection)**: Monitors and protects the application in real-time post-deployment. Refer to `rasp.yml`.

## Essential Considerations

1. **Variables & Configurations**: If specific jobs require secrets or environment variables, set them either in the GitLab project's CI/CD settings or directly in the `.gitlab-ci.yml`.
  
2. **Runner Tags**: Ensure that you have a GitLab runner with the required tags available to pick up and run the jobs.
  
3. **Validation**: Always validate the `.gitlab-ci.yml` using GitLab's CI Lint tool to ensure syntax correctness.
  
4. **Testing**: Trigger test pipelines periodically to make sure all jobs are executing as expected.
